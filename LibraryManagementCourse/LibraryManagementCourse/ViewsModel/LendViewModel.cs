﻿using LibraryManagementCourse.Data.Model;
using System.Collections.Generic;

namespace LibraryManagementCourse.ViewsModel
{
    public class LendViewModel
    {
        public Book Book { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
    }
}
