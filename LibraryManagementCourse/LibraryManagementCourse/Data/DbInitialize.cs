﻿using LibraryManagementCourse.Data.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryManagementCourse.Data
{
    public static class DbInitialize
    {
        public static void Seed(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<LibraryDbContext>();

                var vlad = new Customer { Name = "Vladislaw E" };
                var anna = new Customer { Name = "Anna B" };

                context.Customers.Add(vlad);
                context.Customers.Add(anna);

                var authorDeMarco = new Author
                {
                    Name = "M J deMarco",
                    Books = new List<Book>()
                    {
                        new Book {Title = "The Millionaire Fastlane"},
                        new Book {Title = "Unscripted"}
                    }
                };

                var authorCardone = new Author
                {
                    Name = "Grant Cardone",
                    Books = new List<Book>()
                    {
                        new Book {Title = "Play on Life"},
                        new Book {Title = "Go last stap"},
                        new Book {Title = "Jump with money"}
                    }
                };

                context.Authors.Add(authorDeMarco);
                context.Authors.Add(authorCardone);

                context.SaveChanges();
            }
        }
    }
}
